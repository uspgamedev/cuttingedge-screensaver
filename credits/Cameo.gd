extends Position2D

const SPD = 300

var active = false

export(bool) var skip = false

signal done

func _go(unused):
	self.active = true

func _process(delta):
	if self.active:
		var dir = $Target.position.normalized()
		$PenguinSprite.position += dir*SPD*delta
		$PenguinSprite.set_motion(dir*SPD)
		if $PenguinSprite.position.distance_to($Target.position) < 50:
			emit_signal("done")
			queue_free()
