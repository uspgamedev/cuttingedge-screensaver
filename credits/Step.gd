extends Label

export(int) var left = 0
export(int) var right = 0
export(int) var top = 0
export(int) var bottom = 0

export(float) var pre_delay = 1.0
export(float) var next_delay = 2.0
export(float) var fade_delay = 4.0

export(bool) var wait_fade = false

export(bool) var skip = false

signal done

var can_return = false

func _input(event):
	if can_return:
		get_tree().get_root().get_node('global').load_scn("res://menus/MainMenu.tscn")

func _go(bender):
	self.modulate = Color(1,1,1,0)
	self.visible = true
	if self.name == 'End':
		can_return = true
	yield(get_tree().create_timer(pre_delay), "timeout")
	bender.bend(left, top, right, bottom)
	yield(get_tree().create_timer(0.5), "timeout")
	$Tween.interpolate_property(self, "modulate", Color(1,1,1,0), Color(1,1,1,1), 0.5,
								Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	yield(get_tree().create_timer(next_delay), "timeout")
	if not self.wait_fade:
		emit_signal("done")
	yield(get_tree().create_timer(fade_delay), "timeout")
	$Tween.interpolate_property(self, "modulate", Color(1,1,1,1), Color(1,1,1,0), 0.5,
								Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	if self.wait_fade:
		emit_signal("done")
