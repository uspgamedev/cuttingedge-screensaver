extends Node

const AMOUNT = 50
const MAX_SIZE = Vector2(800, 600)
const MIN_SIZE = Vector2(10, 10)
const PUSH_AMOUNT = 110
const SHRINK_SPEED = -10

enum {LEFT, RIGHT, TOP, BOTTOM}

var delta_side = [0, 0, 0, 0]
var change = [0, 0, 0, 0]
var sides_shrinking = [true, true, true, true]
var gameover = false
var dt = 0

var origin

func _ready():
	OS.window_size = MAX_SIZE
	OS.center_window()

func _process(delta):
	update_window(delta)

func bend(left, top, right, bottom):
	move_side(LEFT, left)
	move_side(RIGHT, right)
	move_side(TOP, top)
	move_side(BOTTOM, bottom)

func move_side(side, amount):
	if side == LEFT or side == RIGHT:
		amount = max(amount, MIN_SIZE.x - OS.window_size.x)
	else:
		amount = max(amount, MIN_SIZE.y - OS.window_size.y)
	amount = min(amount, -delta_side[side])
	change[side] += amount

func update_window(delta):
	var coef = min(1, 10 * delta)
	var change_size = Vector2()
	var change_pos = Vector2()
	var old_size = OS.window_size
	var old_pos = OS.window_position
	if abs(change[LEFT]) > 1:
		var real_change = int(change[LEFT]*coef)
		delta_side[LEFT] += real_change
		change_size.x += real_change
		change_pos.x -= real_change
		change[LEFT] -= real_change
	if abs(change[RIGHT]) > 1:
		var real_change = int(change[RIGHT]*coef)
		delta_side[RIGHT] += real_change
		change_size.x += real_change
		change[RIGHT] -= real_change
	if abs(change[TOP]) > 1:
		var real_change = int(change[TOP]*coef)
		delta_side[TOP] += real_change
		change_size.y += real_change
		change_pos.y -= real_change
		change[TOP] -= real_change
	if abs(change[BOTTOM]) > 1:
		var real_change = int(change[BOTTOM]*coef)
		delta_side[BOTTOM] += real_change
		change_size.y += real_change
		change[BOTTOM] -= real_change
	if change_size != Vector2():
		OS.window_size += change_size
		while OS.window_size == old_size:
			pass
	if change_pos != Vector2():
		OS.window_position += change_pos
		while OS.window_position == old_pos:
			pass
	get_parent().rect_position -= change_pos
	#get_parent().rect_scale = OS.window_size
