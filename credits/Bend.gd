extends Node

export(int) var left = 0
export(int) var right = 0
export(int) var top = 0
export(int) var bottom = 0

export(bool) var skip = false

signal done

func _go(bender):
	bender.bend(left, top, right, bottom)
	yield(get_tree().create_timer(0.3), "timeout")
	emit_signal("done")