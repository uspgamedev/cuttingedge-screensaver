extends Control

const STEP = preload("res://credits/Step.gd")
const BEND = preload("res://credits/Bend.gd")
const CAMEO = preload("res://credits/Cameo.gd")

func _ready():
	go()
	BGM_Manager.play_bgm(BGM_Manager.CREDITS)

func go():
	var children = get_children()
	for child in children:
		if child.has_method("_go") and not child.skip:
			child._go($WindowBender)
			yield(child, "done")
