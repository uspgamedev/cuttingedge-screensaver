extends Node2D

const DASH_PARTICLES = preload("res://penguin-sprite/sensei/DashParticles.tscn")

const DIRS = [
	Vector2(1,1),
	Vector2(-1,1),
	Vector2(1,-1),
	Vector2(-1,-1)
]

const ANIMS = [
	"frontright",
	"frontleft",
	"backright",
	"backleft"
]

const ATK_DIRS = [
	Vector2(1,0),
	Vector2(0,1),
	Vector2(-1,0),
	Vector2(0,-1),
]

const ATK_ANIMS = [
	"right", "down", "left", "up"
]

signal slash_started
signal slash_ended

export var title_pose = false

var last_motion = Vector2()
var last_dir = 0
var attacking = false
var dashing = false
var stunned = false

func _ready():
	if self.title_pose:
		$AnimationPlayer.playback_speed = 0.5
		$AnimationPlayer.current_animation = "title-pose"

func set_motion(dir):
	if self.stunned or self.attacking or self.dashing: return
	var maxdir = 0
	var maxidx = -1
	for i in range(DIRS.size()):
		var align = DIRS[i].dot(dir)
		if align > maxdir:
			maxdir = align
			maxidx = i
			
	if maxidx < 0:
		maxidx = last_dir
	var length = dir.length()
	if length > 10:
		self.last_motion = dir
		$AnimationPlayer.playback_speed = min(1, length/200.0)
		$AnimationPlayer.current_animation = "walk-%s" % ANIMS[maxidx]
	else:
		$AnimationPlayer.playback_speed = 1
		$AnimationPlayer.current_animation = "idle-%s" % ANIMS[maxidx]
	last_dir = maxidx

func dash(dir):
	if self.attacking or self.stunned: return
	$DashSFX.play()
	self.dashing = true
	var dash_particles = DASH_PARTICLES.instance()
	dash_particles.rotation = (-dir).angle()
	dash_particles.emitting = true
	dash_particles.scale.y = 0.6
	dash_particles.position.y -= 50
	add_child(dash_particles)
	$AnimationPlayer.playback_speed = 1
	$AnimationPlayer.current_animation = "dash-%s" % ANIMS[last_dir]
	yield($AnimationPlayer, "animation_finished")
	self.dashing = false

func start_attack():
	if self.dashing: self.dashing = false
	var maxdir = 0
	var maxidx = 0
	var dir = self.last_motion
	for i in range(ATK_DIRS.size()):
		var align = ATK_DIRS[i].dot(dir)
		if align > maxdir:
			maxdir = align
			maxidx = i
#	print("attack %s" % ATK_ANIMS[maxidx])
	$AnimationPlayer.playback_speed = 2
	if self.attacking:
		$AnimationPlayer.stop()
		$AnimationPlayer.play("attack-%s" % ATK_ANIMS[maxidx])
	$AnimationPlayer.current_animation = "attack-%s" % ATK_ANIMS[maxidx]
	self.attacking = true
	yield($AnimationPlayer, "animation_finished")
	self.attacking = false

func _start_slash():
	$SlashSFX.play()
	emit_signal("slash_started")

func _end_slash():
	emit_signal("slash_ended")

func start_stun():
	if self.dashing: self.dashing = false
	self.stunned = true
	$AnimationPlayer.stop()
	self.attacking = false
	self.dashing = false
	$AnimationPlayer.playback_speed = 1
	$AnimationPlayer.current_animation = "stun"
	$StunShockLeft.visible = true
	$StunShockRight.visible = true
	$StunSFX.play()

func end_stun():
	self.stunned = false
	$AnimationPlayer.playback_speed = 1
	$AnimationPlayer.current_animation = "idle-frontright"
	$StunShockLeft.visible = false
	$StunShockRight.visible = false
