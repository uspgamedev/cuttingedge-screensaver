extends Node2D

const SPD = 1000
const ROT = 15

func _ready():
	randomize()
	$GoreSFX.play()
	for child in get_children():
		if child is Sprite:
			child.rotation_degrees = -ROT + randf() * (2 * ROT)
		elif child is RigidBody2D:
			child.get_node("Sprite").scale = Vector2(.1, .1)
			var vel = Vector2((SPD + randi() % SPD) * (-1 + (randi() % 2) * 2), (SPD + randi() % SPD) * (-1 + (randi() % 2) * 2))
			child.set_linear_velocity(vel)
			child.set_angular_velocity(-100 + randf() * 200)
