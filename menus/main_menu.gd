extends Control

func _ready():
	$Center/Menu/Play.grab_focus()
	$Snow.emitting = true
	BGM_Manager.play_bgm(BGM_Manager.MENU)
	if not get_tree().get_root().get_node('global').hd:
		get_node("Center/Menu/Quality").text = 'Low Definition'


func _on_Play_pressed():
	BGM_Manager.play_bgm(BGM_Manager.LEVEL)
	var global = get_tree().get_root().get_node('global')
	global.load_scn(global.level)


func _on_Credits_pressed():
	get_tree().get_root().get_node('global').load_scn("res://credits/Credits.tscn")


func _on_Quit_pressed():
	get_tree().quit()


func _on_Quality_pressed():
	var quality = get_node("Center/Menu/Quality")
	if quality.text == 'High Definition':
		quality.text = 'Low Definition'
		get_tree().get_root().get_node('global').hd = false
	else:
		quality.text = 'High Definition'
		get_tree().get_root().get_node('global').hd = true
	quality._on_MenuButton_focus_entered()
