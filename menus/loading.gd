extends Control

func _ready():
	yield(get_tree(), 'physics_frame')
	yield(get_tree(), 'physics_frame')
	var global = get_tree().get_root().get_node('global')
	get_tree().change_scene(global.scene)
