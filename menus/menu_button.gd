extends Button

const FOCUSED_X = 100
const DURATION = .5


func _on_MenuButton_focus_entered():
	$Tween.interpolate_property(self, "rect_position", rect_position,
		Vector2(FOCUSED_X, rect_position.y), DURATION, Tween.TRANS_ELASTIC,
		Tween.EASE_OUT)
	$Tween.start()

func _on_MenuButton_focus_exited():
	$Tween.interpolate_property(self, "rect_position", rect_position,
		Vector2(0, rect_position.y), DURATION, Tween.TRANS_ELASTIC,
		Tween.EASE_OUT)
	$Tween.start()
