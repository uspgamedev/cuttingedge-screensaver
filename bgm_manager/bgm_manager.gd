extends Node

const IN_DELAY = 0
const IN_DURATION = 1
const OUT_DURATION = 1
const MIN_VOL = -80

enum { MENU, LEVEL, CREDITS }

onready var tween = $Tween
onready var new_bgm = $BGM1
onready var current_bgm = $BGM2

var menu_bgm = preload("res://bgm_manager/Crystal_Cave.ogg")
var level_bgm = preload("res://bgm_manager/Blackmoor_Ninjas_No_Vocals.ogg")
var credits_bgm = preload("res://bgm_manager/Frozen_Jam_Seamless_Loop.ogg")

var bgm_list = [menu_bgm, level_bgm, credits_bgm]

func _ready():
	$BGM1.volume_db = MIN_VOL
	$BGM2.volume_db = MIN_VOL


func play_bgm(new):
	var bgm = bgm_list[new]
	
	if current_bgm.playing:
		fade_out(current_bgm)
	
	new_bgm.stream = bgm
	
	fade_in(new_bgm)
	
	while tween.is_active():
		yield(tween, "tween_completed")
	
	var aux = current_bgm
	current_bgm = new_bgm
	new_bgm = aux


func fade_in(bgm):
	tween.interpolate_property(bgm, "volume_db", bgm.volume_db, 0, IN_DURATION,
		Tween.TRANS_LINEAR, Tween.EASE_IN, IN_DELAY)
	tween.start()
	bgm.play()


func fade_out(bgm):
	tween.interpolate_property(bgm, "volume_db", bgm.volume_db, MIN_VOL,
		OUT_DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
