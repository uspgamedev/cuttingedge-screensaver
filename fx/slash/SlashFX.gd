extends Position2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	self.rotation_degrees = randf()*360
	get_tree().paused = true


func _timeout():
	get_tree().paused = false
	$Tween.interpolate_property(self, "modulate", Color(1,1,1,1), Color(1,1,1,0), .3 ,Tween.TRANS_QUAD, Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_completed")
	queue_free()
