extends Node2D

onready var tween = get_node("Tween")

func _ready():
	tween.interpolate_method(self, "resize_window", Vector2(1024, 600), Vector2(320, 240),
	            1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()

func resize_window(size):
	OS.window_size = size
	OS.center_window()
