extends KinematicBody2D

export var CORPSE = preload("res://entity/Corpse.tscn")

func _die():
	pass

func _spawn_corpse():
	var corpse = CORPSE.instance()
	corpse.position = self.position
	get_parent().add_child(corpse)
	self.queue_free()

func _attacked():
	pass