extends Node2D

onready var active_enemy = null
onready var polygon = get_node("Polygon2D")

func _ready():
	polygon.hide()

func _physics_process(delta):
	if active_enemy == null or active_enemy.get_ref() == null:
		pick_active_enemy()
	else:
		check_distance_of_active_enemy()
	
	if active_enemy != null:
		activate_arrow()
	else:
		deactivate_arrow()

func activate_arrow():
	if not polygon.is_visible():
		polygon.show()
	update_helper_arrow()

func deactivate_arrow():
	active_enemy = null
	polygon.hide()

#Chooses a random enemy outside screen to show its position
func pick_active_enemy():
	active_enemy = null
	
	var player = get_parent()

	for enemy in player.get_parent().get_nodes_in_group(:
		var e_pos = enemy.get_pos()
		var p_pos = player.get_pos()

#If enemy is close enough to player, remove polygon
func check_distance_of_active_enemy():
	var e = active_enemy.get_ref()
	var player = get_parent()
		
	var e_pos = e.get_pos()

func update_helper_arrow():
	var scale = get_node('/root/Main/').get_scale()
	var e = active_enemy.get_ref()
	
	var player = get_parent()
	var camera = player.get_node("Camera")
	
	var e_pos = e.get_pos()
	var p_pos = player.get_pos()

	var w = get_viewport().get_rect().size.x
	var h = get_viewport().get_rect().size.y
	
	
	polygon.set_rot(p_pos.angle_to_point(e_pos))
	
	var angle = p_pos.angle_to_point(e_pos)
	
	var dist = 120
	var center = camera.get_offset()*scale
	var x = center.x + -sin(angle)*dist 
	var y = center.y + -cos(angle)*dist 
	var polygon_pos = Vector2(x, y)
	polygon.set_pos(polygon_pos)
