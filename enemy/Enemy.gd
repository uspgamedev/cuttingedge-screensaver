extends KinematicBody2D

var speed = 120
var playerObject
var isDead = false
var pushDirection = Vector2(0,0)

onready var self_area = get_node('EnemyArea')
onready var player = get_node("../Player")

export (bool) var dont_move = false

const CORPSE = preload("res://enemy/EnemyGore.tscn")
const CLANG_SCN = preload("res://fx/Clang.tscn")

# we USE this
var velocity = Vector2()

func _physics_process(delta):
	var playerPosition = player.position
	var enemyPosition = self.position
	var moveDirection = playerPosition - enemyPosition

	moveDirection = moveDirection.normalized()
		
	if not isDead and not dont_move:
		velocity = moveDirection*speed
		move_and_slide(velocity)
	else:
		velocity = Vector2()
		move_and_slide(velocity)
		
	if $PushTimer.time_left != 0:
		move_and_slide(pushDirection*speed*3*$PushTimer.time_left)
	
	if self_area != null:
		var self_radius = self_area.get_node('CollisionShape2D').shape.radius
		var player_radius = player.get_node('StunArea/CollisionShape2D').shape.radius
		if self_radius + player_radius >= (self.position - player.position).length():
			player.stun(0.5)

func attacked(attacker):
	if not isDead:
		isDead = true
		self_area = null
		$EnemyArea.queue_free()
		self.modulate = Color(200,200,200,1)
		$DeadTween.interpolate_property(self, "modulate", Color(3,3,3,1), Color(1,1,1,1),
										.4, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$DeadTween.start()
		$AnimatedSprite.animation = "dead"
		$AnimatedSprite.connect("animation_finished", self, "_on_death_finished")
	
	knockback(attacker)


func knockback(player):
	var playerPosition = player.position
	var enemyPosition = self.position
	pushDirection = playerPosition - enemyPosition
	pushDirection = -pushDirection.normalized()
	
	$PushTimer.start()


func _on_death_finished():
	if isDead:
		$AnimatedSprite.playing = false
	var corpse = CORPSE.instance()
	corpse.position = self.position
	
	var clang = CLANG_SCN.instance()
	clang.position = (self.position)
	get_parent().add_child(clang)
	
	# happy father's day
	get_parent().add_child(corpse)
	
	self.queue_free()
