extends Position2D

export var delay = 0
export var amount = 5
export(PackedScene) var spawned_scene

func _ready():
	prepare()

func prepare():
	$Timer.wait_time = self.delay + 2.0 + 10*randf()
	$Timer.start()
	self.delay = 0

func _spawn():
	var spawned = spawned_scene.instance()
	spawned.position = self.position
	get_parent().add_child(spawned)
	self.amount -= 1
	if self.amount > 0:
		prepare()
