extends Camera2D

func _process(delta):
	self.limit_right = OS.window_size.x
	self.limit_bottom = OS.window_size.y
	self.zoom += 10 * (Vector2(1, 1) - self.zoom) * delta

func focus():
	self.zoom = Vector2(.5, .5)
