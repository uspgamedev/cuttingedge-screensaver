extends Node2D

onready var active_enemy
onready var polygon = get_node("Polygon2D")
onready var player = get_parent()
var target_rotation

func _ready():
	polygon.hide()
	
func _physics_process(delta):
	pick_active_enemy()
	
	if active_enemy != null:
		activate_arrow(delta)
	else:
		deactivate_arrow()

func activate_arrow(delta):
	if not polygon.is_visible():
		polygon.show()
	update_helper_arrow(delta)

func deactivate_arrow():
	active_enemy = null
	polygon.hide()

func pick_active_enemy():
	active_enemy = null
	
	var min_dist = INF

	for enemy in player.get_parent().get_children():
		if enemy.is_in_group('enemies') and not enemy.is_in_group('gibs'):
			var e_pos = enemy.position
			var p_pos = player.position
			if (p_pos - e_pos).length() < min_dist:
				min_dist = (p_pos - e_pos).length()
				active_enemy = weakref(enemy)
	
	if active_enemy:
		var e = active_enemy.get_ref()
		
		var e_pos = e.position
		var p_pos = player.position
		target_rotation = p_pos.angle_to_point(e_pos) - PI/2

func update_helper_arrow(delta):
	polygon.rotation += (target_rotation - polygon.rotation)*delta*10
	