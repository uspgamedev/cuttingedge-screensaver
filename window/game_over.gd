extends Control

func _ready():
	set_process_input(false)


func _input(event):
	if event.is_action_pressed('ui_cancel'):
		get_tree().get_root().get_node('global').load_scn("res://menus/MainMenu.tscn")
	elif event.is_action_pressed('dash_action'):
		get_tree().reload_current_scene()

func start():
	var blood = $BloodyText
	var tween = $Tween
	tween.interpolate_property(blood, "scale", Vector2(), Vector2(1, 1), 1,
		Tween.TRANS_BACK, Tween.EASE_OUT)
	tween.interpolate_property(blood, "modulate", Color(1, 1, 1, 0),
		Color(1, 1, 1, 1), 1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	blood.show()
	
	yield(tween, "tween_completed")
	# Bloody Game Over is dripping on screen now
	set_process_input(true)
	
	tween.interpolate_property(blood, "position", blood.position,
		Vector2(blood.position.x, blood.position.y + 1200), 20,
		Tween.TRANS_QUAD, Tween.EASE_IN)
	tween.interpolate_property(blood, "scale", blood.scale, Vector2(1, 4), 20,
		Tween.TRANS_QUAD, Tween.EASE_IN)
	tween.interpolate_property($CanvasLayer/PressKey, "modulate", Color(1, 1, 1, 0),
		Color(1, 1, 1, 1), .5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()