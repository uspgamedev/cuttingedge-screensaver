extends StaticBody2D

export(int, "Left", "Right", "Top", "Bottom") var side

signal stop_timeout
signal was_attacked

var cont = 1

onready var window_manager = get_node('../../')
onready var shader = get_node('BackBufferCopy/Shader')

var time_ = 0
func _physics_process(delta):
	if get_tree().get_root().get_node('global').hd:
		var step = 0.05
		time_+= delta
		if (time_ >= step):
			shader.material.set_shader_param('displacement', window_manager.image_vec[cont])
			cont += 1
			if cont == 60:
				cont = 1
			time_ = time_ - step

func attacked(amount):
	emit_signal("was_attacked", side, amount)


func stop_sfx():
	$DragSFX.stop()


func start_timer():
	$Timer.start()


func _on_Timer_timeout():
	emit_signal("stop_timeout", side)
