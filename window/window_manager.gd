extends Node2D

const AMOUNT = 50
const MAX_SIZE = Vector2(800, 600)
const MIN_SIZE = Vector2(30, 30)
const PUSH_AMOUNT = 110
export var SHRINK_SPEED = -10
export (float) var SHRINK_TIME = 1

enum {LEFT, RIGHT, TOP, BOTTOM}

onready var top_wall = $Walls/Top
onready var bottom_wall = $Walls/Bottom
onready var left_wall = $Walls/Left
onready var right_wall = $Walls/Right
var delta_side = [0, 0, 0, 0]
var change = [0, 0, 0, 0]

export var shrink_left = true
export var shrink_right = true
export var shrink_top = true
export var shrink_bottom = true
var sides_shrinking = [true, true, true, true]

var gameover = false
var dt = 0
var image_vec = []
var gibs = []

func _ready():
	OS.window_size = MAX_SIZE
	OS.center_window()
	set_process_input(false)
	
	if get_tree().get_root().get_node('global').hd:
		for i in range(1, 61):
			var tex = load('res://blender/shader/00%02d' %[i] + '.png')
			image_vec.append(tex)
	else:
		get_node("Walls/Top/BackBufferCopy").visible = false
		get_node("Walls/Left/BackBufferCopy").visible = false
		get_node("Walls/Right/BackBufferCopy").visible = false
		get_node("Walls/Bottom/BackBufferCopy").visible = false

func _process(delta):
	for side in [LEFT, RIGHT, TOP, BOTTOM]:
		if sides_shrinking[side] and should_shrink(side):
			move_side(side, SHRINK_SPEED * delta)
	
	update_window(delta)
	
	update_walls()


func should_shrink(side):
	if side == LEFT:
		return shrink_left
	if side == RIGHT:
		return shrink_right
	if side == TOP:
		return shrink_top
	if side == BOTTOM:
		return shrink_bottom

func push_side(side, amount):
	sides_shrinking[side] = false
	var name = ["Left", "Right", "Top", "Bottom"]
	get_node("Walls/" + name[side]+"/Timer").wait_time = SHRINK_TIME
	get_node("Walls/" + name[side]).start_timer()
	$PushSFX.play()
	move_side(side, amount)


func move_side(side, amount):
	if side == LEFT or side == RIGHT:
		amount = max(amount, MIN_SIZE.x - OS.window_size.x)
	else:
		amount = max(amount, MIN_SIZE.y - OS.window_size.y)
	amount = min(amount, -delta_side[side])
	change[side] += amount

func is_not_maximized():
	for side in [LEFT, RIGHT, TOP, BOTTOM]:
		if abs(delta_side[side]) >= 2:
			return true
	return false


func update_walls():
	top_wall.position = Vector2(OS.window_size.x / 2, -100)
	bottom_wall.position = Vector2(OS.window_size.x / 2, OS.window_size.y + 100)
	left_wall.position = Vector2(-100, OS.window_size.y / 2)
	right_wall.position = Vector2(OS.window_size.x + 100, OS.window_size.y / 2)


func update_window(delta):
	var coef = 1
	if delta:
		coef = min(1, 10 * delta) 
	var change_size = Vector2()
	var change_pos = Vector2()
	var old_size = OS.window_size
	var old_pos = OS.window_position
	if abs(change[LEFT]) > 1:
		var real_change
		if sign(change[LEFT]) == 1:
			real_change = max(1,int(change[LEFT]*coef))
		else:
			real_change = min(-1,int(change[LEFT]*coef))
		delta_side[LEFT] += real_change
		change_size.x += real_change
		change_pos.x -= real_change
		change[LEFT] -= real_change
	if abs(change[RIGHT]) > 1:
		var real_change
		if sign(change[RIGHT]) == 1:
			real_change = max(1,int(change[RIGHT]*coef))
		else:
			real_change = min(-1,int(change[RIGHT]*coef))
		delta_side[RIGHT] += real_change
		change_size.x += real_change
		change[RIGHT] -= real_change
	if abs(change[TOP]) > 1:
		var real_change
		if sign(change[TOP]) == 1:
			real_change = max(1,int(change[TOP]*coef))
		else:
			real_change = min(-1,int(change[TOP]*coef))
		delta_side[TOP] += real_change
		change_size.y += real_change
		change_pos.y -= real_change
		change[TOP] -= real_change
	if abs(change[BOTTOM]) > 1:
		var real_change
		if sign(change[BOTTOM]) == 1:
			real_change = max(1,int(change[BOTTOM]*coef))
		else:
			real_change = min(-1,int(change[BOTTOM]*coef))
		delta_side[BOTTOM] += real_change
		change_size.y += real_change
		change[BOTTOM] -= real_change
	if change_size != Vector2():
		OS.window_size += change_size
		while OS.window_size == old_size:
			pass
	if change_pos != Vector2():
		OS.window_position += change_pos
		for body in $Bodies.get_children():
			body.position -= change_pos
		get_node("../TextureRect").rect_position -= change_pos
		while OS.window_position == old_pos:
			pass


func reset_window(lost=true):
	gameover = true
	sides_shrinking = [false, false, false, false]
	
	for side in [LEFT, RIGHT, TOP, BOTTOM]:
		move_side(side, -delta_side[side])
	for wall in $Walls.get_children():
		wall.stop_sfx()
	
	if lost:
		$GameOver.start()
	if not lost:
		$EndTimer.start()
	
	get_parent().get_node("CanvasLayer/Control").hide()
	get_parent().get_node("CanvasLayer/Control/Display/WinTimer").stop()


func _on_Timer_timeout(side):
	if not gameover:
		sides_shrinking[side] = true


func _on_Wall_was_attacked(side, amount = PUSH_AMOUNT):
	push_side(side, amount)
