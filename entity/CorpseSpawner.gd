extends Node2D

const SPD = 100
const ROT = 15

var CHILDREN = []

func _ready():
	randomize()
	for child in get_children():
		if child is Sprite:
			child.rotation_degrees = -ROT + randf() * (2 * ROT)
		elif child.is_in_group('gibs'):
			var vel = Vector2((SPD + randi() % SPD) * (-1 + (randi() % 2) * 2), (SPD + randi() % SPD) * (-1 + (randi() % 2) * 2))
			child.velocity = vel
			child.angular  = (-100 + randf() * 200)
	$remove_children.start()

func _on_remove_children_callback():
	for child in get_children():
		if child.is_in_group('gibs'):
			CHILDREN.append(child)
			self.remove_child(child)
	$give_parent_children.start()

func _on_give_parent_children_callback():
	for child in CHILDREN:
		var window_manager = get_node('../../')
		var global = get_tree().get_root().get_node('global')
		get_parent().add_child(child)
		if (global.hd and window_manager.gibs.size() > window_manager.get_parent().HQ_SHARDS) or \
		   (not global.hd and window_manager.gibs.size() > window_manager.get_parent().LQ_SHARDS):
			window_manager.gibs.pop_front().queue_free()
		window_manager.gibs.append(child)
		child.position = self.position
	self.queue_free()
