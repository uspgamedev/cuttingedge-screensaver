extends KinematicBody2D

var velocity = Vector2(0,0)
var angular  = 0.0
export var v_damp   = 0.015
export var a_damp   = 0.015

var isDead = true # needed
var push_vec = []

export var radius = 20.0

const SWORD_FACTOR = 5000
const KICK_FACTOR = 0.1
const SMALL = Vector2(1,0)*10

func attacked(attacker):
	knockback(attacker)

func knockback(player):
	var vec = (self.position.normalized() - player.position.normalized()).normalized()*SWORD_FACTOR
	push_vec.append(vec)

func _physics_process(delta):
	velocity -= velocity*v_damp
	angular  -= angular*a_damp
	
	for push in push_vec:
		velocity += push*delta
	push_vec = []
	
	for child in get_children():
		child.rotation += angular*delta

	var colliders = get_tree().get_nodes_in_group('player')
	for body in colliders:
		var r = (self.position - body.position)
		if  r.length() < body.get_node("CollisionShape2D").shape.radius + radius:
			var v = body.velocity
			var tang = Vector2(r.y, -r.x).normalized()
			velocity+= v.dot(r.normalized())*r.normalized()*KICK_FACTOR
			angular += tang.dot(v)/r.length()*KICK_FACTOR
	
	var col_data = move_and_collide(velocity*delta)
	if  col_data == null:
		return

	var normal   = col_data.normal
	var tangent  = Vector2(normal.y, -normal.x).normalized()
	
	velocity     = velocity.dot(tangent)*tangent - velocity.dot(normal)*normal
	angular     +=  tangent.dot(velocity)/position.length()
