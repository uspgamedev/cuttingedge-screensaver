extends Label

func modulo(x, mod):
	var result = x
	if (result > 0):
		while (result > 0):
			result -= mod
		result += mod
	else:
		while (result < 0):
			result += mod
		result -= mod
	return result

func _process(delta):
	var time   = $WinTimer.time_left
	var second = int(modulo(time, 60))
	var minute = int(time - second)/60
	
	text = "%02d'%02d''" % [minute, second]