extends Node2D

export(String, FILE, "*.tscn") var next_level
export(int) var LQ_SHARDS = 50
export(int) var HQ_SHARDS = 100

func _ready():
	if get_tree().get_root().get_node('global').hd:
		$Snow.emitting = true
	else:
		$Snow.visible = false

func win():
	var global = get_tree().get_root().get_node('global')
	if next_level:
		global.load_scn(next_level)
		global.level = next_level
	else:
		global.load_scn("res://credits/Credits.tscn")

func _on_WinTimer_timeout():
	$WindowManager.reset_window(false)
	$WindowManager/Bodies/Player.set_physics_process(false)
	yield($WindowManager/EndTimer, 'timeout')
	get_tree().paused = true
	
	var tw = $CanvasLayer/WinLabel/Tween
	tw.interpolate_property($CanvasLayer/WinLabel, "modulate", Color(1, 1, 1, 0),
		Color(1, 1, 1, 1), 1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tw.start()
	
	yield(tw, "tween_completed")
	$CanvasLayer/WinLabel/DisplayTimer.start()
	
	yield($CanvasLayer/WinLabel/DisplayTimer, "timeout")
	get_tree().paused = false
	win()
